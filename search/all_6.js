var searchData=
[
  ['generate_29',['generate',['../server_8h.html#aca9e6ca82c7ed528b0df514816f38f74',1,'server.c']]],
  ['generate_5fboard_30',['generate_board',['../board_8h.html#a43b1260c41941bf8dc0fe90a61fbf072',1,'board.c']]],
  ['get_31',['get',['../bfs_8h.html#abc75e22e87165d40c7aa5b1b3152f47a',1,'bfs.c']]],
  ['get_5fwinner_32',['get_winner',['../server_8h.html#a65397a510eced3505792e700f66adf98',1,'server.c']]],
  ['graph_5fdisp_33',['graph_disp',['../display_8h.html#abbdf5001aa41d45bbe4ca3b9218bd859',1,'display.c']]],
  ['graph_5ft_34',['graph_t',['../structgraph__t.html',1,'']]],
  ['graph_5ftype_35',['graph_type',['../display_8h.html#a4f164715f9ba9f1c0547df1ef57b1559',1,'display.c']]]
];
