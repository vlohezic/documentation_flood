var server_8h =
[
    [ "bfs_counter_server", "server_8h.html#aecd4c4ca24d0bbc0bb5eb408d2866b8d", null ],
    [ "generate", "server_8h.html#aca9e6ca82c7ed528b0df514816f38f74", null ],
    [ "get_winner", "server_8h.html#a65397a510eced3505792e700f66adf98", null ],
    [ "is_tag", "server_8h.html#a6a34d56e9a5f42ae1788537083fb9c93", null ],
    [ "nzmax", "server_8h.html#a784d8172fe2b3034810901b6ca660492", null ],
    [ "random_player", "server_8h.html#a412d1f79c94dd950ee1c1150f657f050", null ],
    [ "st_cmp_server", "server_8h.html#a5fd1e5d970a0223951526bc83beead73", null ],
    [ "st_copy_server", "server_8h.html#a8663abbd42c82f35d120bed33e22ff52", null ],
    [ "st_del_server", "server_8h.html#acdb5217fd43a60f8f13099a9e0b0cfcd", null ],
    [ "update_graph_server", "server_8h.html#a0c12d0ea552473871e10dc8f715924d0", null ]
];